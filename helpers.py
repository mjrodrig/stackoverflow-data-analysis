'''

This file is intended to contain helpers functions to be imported in all the notebooks or scripts.

'''

import re
import pandas as pd
from pandas.api.types import is_numeric_dtype
import pycountry as pc
import pycountry_convert as pcc
import seaborn as sns
import matplotlib.pyplot as plt
import textwrap

#Helpers and functions:



CLEANR = re.compile('<.*?>') 

def cleanhtml(raw_html):
  cleantext = re.sub(CLEANR, '', raw_html)
  return cleantext


def select_top_n(ps,n=5):
    '''
    Note it takes a pandas Series, not a dataframe!!
    '''
    return ps.dropna().value_counts().head(n).index.tolist()

def get_country(name, alpha_3=False, continent=False):
    country = pc.countries.get(name=name.replace('...',''))
    if country == None:
        country = pc.countries.get(official_name=name)
    if country == None:
        country = pc.countries.get(common_name=name)
    if alpha_3:
        if country == None:
            return None
        else:
            return country.alpha_3
    if continent:
        if country == None:
            return None
        else:
            return pcc.convert_continent_code_to_continent_name(
                pcc.country_alpha2_to_continent_code(
                    country.alpha_2
                )
            )
    return country

def df_row_normalize(dataframe):
    return dataframe.div(dataframe.sum(axis=1), axis=0)


def plt_annotate(ax, total):
    for p in ax.patches:
        per = p.get_width()*100/total
        ax.annotate('{:.2f} %'.format(per), (p.get_width(), p.get_y()+p.get_height()/2))

def plt_wraplabel(ax):

    labelsx = [textwrap.fill(label.get_text(), 20) for label in ax.get_xticklabels()]
    ax.set_xticklabels(labelsx)
    labelsy = [textwrap.fill(label.get_text(), 20) for label in ax.get_yticklabels()]
    ax.set_yticklabels(labelsy)

    return ax

def plotbar_multi(data, colname, order=False, custom_order='', palette=sns.color_palette("rocket_r")):
    if is_numeric_dtype(data[colname]):
        raise ValueError("This tool is to plot categorical data.")
    else:
        if order == 'custom':
            ax = sns.countplot(data=data,
                          y=data[colname].str.split(';', expand=True).stack(),
                          order=custom_order,
                          palette=palette,
                          orient='h')
        elif order is True:
            ax = sns.countplot(data=data,
                          y=data[colname].str.split(';', expand=True).stack(),
                          order=data[colname].str.split(';', expand=True).stack().value_counts(normalize=True).index,
                          palette=palette,
                          orient='h')
        else:
            ax = sns.countplot(data=data,
                          y=data[colname].str.split(';', expand=True).stack(),
                          palette=palette,                          
                          orient='h')
            
    plt_annotate(ax,data[colname].dropna().count())
    labelsy = [textwrap.fill(label.get_text(), 30) for label in ax.get_yticklabels()]
    ax.set_yticklabels(labelsy)
    # plt.show()
    return ax

    
def df_explode(df, colname):
    aux = df.copy()
    aux.loc[:,colname] = aux[colname].dropna().str.split(';')
    aux = aux.explode(colname)
    return aux

def prepare_data(data,data_schema):

    data_schema['question'] = data_schema['question'].apply(cleanhtml)

    data_schema = pd.concat([data_schema,pd.DataFrame(
        { 'qname' : ['LanguageHaveWorkedWith'        ,
                        'LanguageWantToWorkWith'        ,
                        'DatabaseHaveWorkedWith'        ,
                        'DatabaseWantToWorkWith'        ,
                        'PlatformHaveWorkedWith'        ,
                        'PlatformWantToWorkWith'        ,
                        'WebframeHaveWorkedWith'        ,
                        'WebframeWantToWorkWith'        ,
                        'MiscTechHaveWorkedWith'        ,
                        'MiscTechWantToWorkWith'        ,
                        'ToolsTechHaveWorkedWith'       ,
                        'ToolsTechWantToWorkWith'       ,
                        'NEWCollabToolsHaveWorkedWith'  ,
                        'NEWCollabaToolsWantToWorkWith',
                        'Continent'],
        'question': ['Language Have Worked With'        ,
                        'Language Want To Work With'        ,
                        'Database Have Worked With'        ,
                        'Database Want To Work With'        ,
                        'Platform Have Worked With'        ,
                        'Platform Want To Work With'        ,
                        'Webframe Have Worked With'        ,
                        'Webframe Want To Work With'        ,
                        'MiscTech Have Worked With'        ,
                        'MiscTech Want To Work With'        ,
                        'ToolsTech Have Worked With'       ,
                        'ToolsTech Want To Work With'       ,
                        'CollabTools Have Worked With'  ,
                        'CollabaTools Want To Work With',
                        'Continent']
        }
    )])

    data_schema.loc[data_schema['qname'] == 'SOPartFreq','question'] = 'How frequently would you say you participate in Q&A on Stack Overflow? By participate we mean ask, answer, vote for, or comment on questions.'

    # Some contries are wrongly written, let's clean them up:

    data.loc[data['Country'] == 'Cape Verde','Country'] = 'Cabo Verde'
    data.loc[data['Country'] == 'Hong Kong (S.A.R.)','Country'] = 'Hong Kong'
    data.loc[data['Country'] == 'The former Yugoslav Republic of Macedonia','Country'] = 'North Macedonia'
    data.loc[data['Country'] == 'Republic of Korea','Country'] = 'Korea, Republic of'
    data.loc[data['Country'] == 'Palestine','Country'] = 'Palestine, State of'
    data.loc[data['Country'] == 'Democratic Republic of the Congo','Country'] = 'Congo, The Democratic Republic of the'
    data.loc[data['Country'] == 'Congo, Republic of the...','Country'] = 'Congo'
    data.loc[data['Country'] == 'Libyan Arab Jamahiriya','Country'] = 'Libya'
    data.loc[data['Country'] == 'Swaziland','Country'] = 'Eswatini'


    data['Alpha_3'] = data['Country'].apply(get_country,alpha_3=True)

    #Kosovo is not on pycontries
    data.loc[data['Country'] == 'Kosovo','Alpha_3'] = 'KOS'
    #Nomadic cannot be plotted

    #And add the continent to make some more demographics:

    data['Continent'] = data['Country'].apply(get_country,continent=True)

    #Order some columns
    data['Age'] = pd.Categorical(data['Age'], ['Under 18 years old',
                                                '18-24 years old',
                                                '25-34 years old',  
                                                '35-44 years old',
                                                '45-54 years old', 
                                                '55-64 years old',
                                                '65 years or older', 
                                                'Prefer not to say'],
                                                ordered=True)
    data['EdLevel'] = pd.Categorical(data['EdLevel'], ['Primary/elementary school',
                                                'Secondary school (e.g. American high school, German Realschule or Gymnasium, etc.)',
                                                'Bachelor’s degree (B.A., B.S., B.Eng., etc.)',
                                                'Master’s degree (M.A., M.S., M.Eng., MBA, etc.)',
                                                'Other doctoral degree (Ph.D., Ed.D., etc.)',
                                                'Some college/university study without earning a degree', 
                                                'Professional degree (JD, MD, etc.)',
                                                'Associate degree (A.A., A.S., etc.)',
                                                'Something else'],
                                                ordered=True)
    data['Employment'] = pd.Categorical(data['Employment'], ['Employed full-time', 
                                                        'Employed part-time',
                                                        'Independent contractor, freelancer, or self-employed',
                                                        'Not employed, but looking for work',
                                                        'Not employed, and not looking for work',
                                                        'Student, full-time',
                                                        'Student, part-time',
                                                        'Retired',
                                                        'I prefer not to say'],
                                                        ordered=True)
    data['SOVisitFreq'] = pd.Categorical(data['SOVisitFreq'], ['Multiple times per day', 
                                                                'Daily or almost daily',
                                                                'A few times per week', 
                                                                'A few times per month or weekly', 
                                                                'Less than once per month or monthly'],
                                                                ordered=True)
    data['SOPartFreq'] = pd.Categorical(data['SOPartFreq'], ['Multiple times per day',
                                                            'Daily or almost daily',
                                                            'A few times per week',
                                                            'A few times per month or weekly',
                                                            'Less than once per month or monthly',
                                                            'I have never participated in Q&A on Stack Overflow'],
                                                   ordered=True)
    data['SOComm'] = pd.Categorical(data['SOComm'], ['Yes, definitely',  
                                                    'Yes, somewhat',
                                                    'Neutral', 
                                                    'No, not really',
                                                    'No, not at all',
                                                    'Not sure'],
                                                   ordered=True)

    return data, data_schema